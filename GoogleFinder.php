<?php
error_reporting(E_ERROR);

require_once dirname(__FILE__) . '/simple_html_dom.php';
require_once dirname(__FILE__) . '/PRclass.php';
require_once dirname(__FILE__) . '/IFclass.php';
require_once dirname(__FILE__) . '/int.php';

class GoogleFinder {

	public function getPR($link) {
		$pr = new PR();
		return (int)$pr->get_google_pagerank($link);
	}

	public function getYear($link) {
		$age = "";
		$domain = parse_url($link); 
		$ageurl = 'http://www.webconfs.com/domain-age.php';
		$fields = array (
			'domains' => $domain['host'],
			'submit' => 'submit'
		);
		$fieldstr = http_build_query($fields);
					
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $ageurl);
		curl_setopt($ch,CURLOPT_POST, 1);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fieldstr);

		$result = curl_exec($ch);
		$htmlage = str_get_html($result);

		curl_close($ch);
		foreach($htmlage->find('a[title*="this Age"]') as $a) {
			$age = $a->plaintext;
		}
					
		$year = date("Y") - int($age);
		
		return $year;
	}

	public function getMultipleYear($links) {
		$counter = 0;
		$domains_str = "";
		$domains10 = array();
		$years10 = array();
		$results = array();

		foreach ($links as $link) {
#			echo "###### " . $counter . "#######" . $counter%10 . "######\n";
#				echo $link . "\n";

			$domain = parse_url($link['link']); 
			array_push($domains10, $link['link']);
			$domains_str .= $domain['host'] . "\n";

			if (++$counter%10 === 0 || $counter == count($links)) {
#				echo $counter . "  ---- 10 get\n";
#				echo $domains_str;

				$ageurl = 'http://www.webconfs.com/domain-age.php';
				$fields = array (
					'domains' => $domains_str,
					'submit' => 'submit'
				);
				$fieldstr = http_build_query($fields);
					
				$ch = curl_init();
				curl_setopt($ch,CURLOPT_URL, $ageurl);
				curl_setopt($ch,CURLOPT_POST, 1);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch,CURLOPT_POSTFIELDS, $fieldstr);

				$result = curl_exec($ch);
				curl_close($ch);

				$htmlage = str_get_html($result);

				foreach($htmlage->find('a[title*="this Age"]') as $a) {
						$age = $a->plaintext;
						$year = date("Y") - int($age);
						array_push($years10, $year);
				}
					
				for ($j = 0; $j < count($domains10); $j++) {
					$results[$domains10[$j]] = $years10[$j];
				}

				unset($domains_str);
				unset($domains10);
				unset($years10);

				$years10 = array();
				$domains10 = array();
#				echo $link . "\n";
			}
		}

#		print_r($results);
		return $results;
	}

	public function getAlexa($link) {
		$domain = parse_url($link);
		$xml = simplexml_load_file('http://data.alexa.com/data?cli=10&dat=snbamz&url='.$domain['host']);
		$alexarank=null;
		
		foreach($xml->SD as $sd) {
			if(isset($sd->POPULARITY)) {
				$alexarank = intval($sd->POPULARITY->attributes()->TEXT);
			}
		}
		return $alexarank;
	}

	public function getThumbnail($link) {
		$finder = new ImageFinder($link);
		$thumbnails = $finder->get_images();

		return $thumbnails[0]['src'];
	}

	public function getLinks($keywords, $thumbnail = true) {
		$start = 0;
		$step = 8;
		$links = array();

		
		while ($start < 64) {
			sleep(rand(1,3));
			$google_url  = 'https://ajax.googleapis.com/ajax/services/search/web?v=1.0' 
									. '&q=' . rawurlencode($keywords)
									. '&rsz=' . $step
									//. "&oq=" . rawurlencode($keywords)
									. "&start=" . $start;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $google_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_REFERER, $_SERVER['SERVER_ADDR']);
			//curl_setopt($ch, CURLOPT_REFERER, "http://tanner.crabdance.com");
			$body = curl_exec($ch);
			$json = json_decode($body, true);
			curl_close($ch);
			
			if(!is_array($json['responseData']['results'])) print_r($body);
			foreach ($json['responseData']['results'] as $linkObj) {
				$link = $linkObj['url'];
				$title = $linkObj['titleNoFormatting'];
				$desc = strip_tags($linkObj['content']);
    
				// if it is not a direct link but url reference found inside it, then extract
				if (!preg_match('/^https?/', $link) && preg_match('/q=(.+)&amp;sa=/U', $link, $matches)
					&& preg_match('/^https?/', $matches[1])) {
        					$link = $matches[1];
				} else if (!preg_match('/^https?/', $link)) { // skip if it is not a valid link
					continue;    
    				}
				
				if ( $thumbnail ) {
					//$thumblink = $this->getThumbnail($link);
					$links[$link] = array('link' => $link, 'title' => $title, 
										'desc' => $desc,
										'thumbnail' => $thumblink
									);
				}

				else {
					$links[$link] = array('link' => $link, 'title' => $title, 
										'desc' => $desc,
									);
				}
			}

			$start += $step;
		}

		
		
		return $links;
	}
}

#$gf = new GoogleFinder();
#$gf->getMultipleYear($gf->getLinks("vim"));
#$gf->getMultipleYear($links);
?>
