<?php
require_once 'simple_html_dom.php';
require_once 'PRclass.php';
function int($s){return($a=preg_replace('/[^\-\d]*(\-?\d*).*/','$1',$s))?$a:'0';}

class GoogleFinder {
	private $links = array();
	private $rank;
	private $year;
	private $alexarank;

	private function getPR($link) {
		return $this->links[$link]['rank']; 
	}

	private function getYear($link) {
		return $this->links[$link]['year'];
	}

	private function getAlexa($link) {
		return $this->links[$links]['alexa'];
	}

	public function getLinks($keywords, $min_pr = 4, $max_age = 2008, $max_alexa = 50000) {
		$start = 0;

		$pr = new PR();
		$ch = curl_init();

		while ($start != 100) {
			$google_url  = 'http://www.google.com/search?hl=en&safe=active&tbo=d&site=&source=hp' 
									. '&q=' . rawurlencode($keywords)
									. "&oq=" . rawurlencode($keywords)
									. "&start=" . $start;
			$google_html = file_get_html($google_url);

			$linkObjs = $google_html->find('h3.r a');
			foreach ($linkObjs as $linkObj) {
				$title = trim($linkObj->plaintext);
				$link  = trim($linkObj->href);
    
				// if it is not a direct link but url reference found inside it, then extract
				if (!preg_match('/^https?/', $link) && preg_match('/q=(.+)&amp;sa=/U', $link, $matches)
					&& preg_match('/^https?/', $matches[1])) {
        					$link = $matches[1];
				} else if (!preg_match('/^https?/', $link)) { // skip if it is not a valid link
					continue;    
    				}

				$this->rank = (int)$pr->get_google_pagerank($link);
				$domain = parse_url($link); 

#    $gindexurl = 'http://www.northcutt.com/tools/free-seo-tools/google-indexed-pages-checker/';
#    $fields = array (
#	    'f_url' => $domain['host'],
#	    'a' => 'submit'
#    );
#    $fieldstr = http_build_query($fields);

#    curl_setopt($ch,CURLOPT_URL, $gindexurl);


    
				if ( $this->rank >= 4 ) {
					$age = "";
					$ageurl = 'http://www.webconfs.com/domain-age.php';
					$fields = array (
						'domains' => $domain['host'],
						'submit' => 'submit'
					);
					$fieldstr = http_build_query($fields);


					curl_setopt($ch,CURLOPT_URL, $ageurl);
					curl_setopt($ch,CURLOPT_POST, 1);
					curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch,CURLOPT_POSTFIELDS, $fieldstr);

					$result = curl_exec($ch);
					$htmlage = str_get_html($result);

					foreach($htmlage->find('a[title*="this Age"]') as $a) {
						$age = $a->plaintext;
					}
	    
					$this->year = date("Y") - int($age);

					$xml = simplexml_load_file('http://data.alexa.com/data?cli=10&dat=snbamz&url='.$domain['host']);
					$this->alexarank=isset($xml->SD[1]->POPULARITY)?$xml->SD[1]->POPULARITY->attributes()->TEXT:0;

					if( $this->year <= $max_age && $this->alexarank >= $max_alexa ) {
						$this->links[$link] = array('link' => $link, 'title' => $title, 'rank' => $this->rank, 
					    						'year' => $this->year,
											'alexa' => (int)$this->alexarank
									);
					}
				}
			}

			$start += 10;
		}

		curl_close($ch);

		return $this->links;
	}
}

#$gf = new GoogleFinder();
#print_r($gf->getLinks("katty perry"));
?>
